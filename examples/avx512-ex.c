#include <stdio.h>
#include <immintrin.h>

void print(char *name, unsigned long long *a, int num)

{

int i;

printf("%s =%ul",name,a[0]);

for (i = 1; i < num; i++)

printf("\n%llu",a[i]);

printf("\n");

}

int main(int argc, char *argv[]) {

    unsigned long long a[] = { 0xFFFFFFFFFFFFFFFF, 0xFFFFFFF2FFFFFFF1, 0xFFFFFFF1FFFFFFF1, 0xFFFFFFF1FFFFFFF1 , 0xFFFFFFF1FFFFFFF1, 0x1FFFFFF1FFFFFFF1, 0xFFFFFFF1FFFFFFF1, 0xFFFFFFF1FFFFFFF1};
    unsigned long long b[] = { 0x1FFFFFF1FFFFFFFF, 0xFFFFFFF2FFFFFFF1, 0xFFFFFFF1FFFFFFF1, 0x2FFFFFF1FFFFFFF1 , 0x1FFFFFF1FFFFFFF1, 0x1FFFFFF1FFFFFFF1, 0xFFFFFFF1FFFFFFF1, 0xFFFFFFF1FFFFFFF1};
    unsigned long long c[] = {0,0,0,0,0,0,0,0};
    __m512i simd1, simd2, res;
    simd1 = _mm512_load_epi64(a);
    simd2 = _mm512_load_epi64(b);
    res = _mm512_add_epi64(simd1, simd2);
    _mm512_store_epi64(c, res);

    print("a+b = " , c , 8);

    return 0;
}