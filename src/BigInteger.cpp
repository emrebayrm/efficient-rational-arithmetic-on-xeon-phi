#include "BigInteger.h"
#include <vector>
#include <cstring>
#include <bits/unique_ptr.h>
#include <iostream>
#include <cmath>
#include <utility>

#define CREATE_ZERO (new BigInteger("0"))

BigInteger::BigInteger(std::string inp){
    int len = inp.length();
    const char *inp_p;
    int index = 0;
    this->size = 0;
    inp_p = inp.c_str();
    while(inp_p[index] == ' ') //skip whitespace
        ++index; 
    if(inp_p[index] == '-'){
        setSign(MINUS);
        ++index;
    }
    else if(inp_p[index] == '+'){
        setSign(PLUS);
        ++index;
    }
    else
        setSign(PLUS);
    while(index < len && (inp_p[index] == ' ' || inp_p[index] == '0')) //skip whitespace and zeros
        ++index;
    if((len - index) / MAX_ORDER != 0 && (len - index) % MAX_ORDER == 0)
        this->size = (len - index) / MAX_ORDER;
    else
        this->size = (len - index) / MAX_ORDER + 1;
    this->number_data = new largeinteger_t[((this->size / 8) + 1) * 8];
    memset(this->number_data, 0, sizeof(largeinteger_t) * ((this->size / 8) + 1) * 8);
    if(index == len){
        return ;
    }
    int i = len - MAX_ORDER;
    int j = 0;
    i = i < 0 ? index : i;
    while( i >= index ) {
        largeinteger_t numb;
        numb = strtoull(inp.substr(i, MAX_ORDER).c_str(), nullptr ,10);
        this->number_data[j] = numb;
        i -= MAX_ORDER;
        ++j;
        if(i < index){
            numb = strtoull(inp.substr(index, i + MAX_ORDER - index ).c_str(), nullptr, 10);
            this->number_data[j] = numb;
        }
    }

}



BigInteger::BigInteger(largeinteger_t *data, int size, SIGN sign){
    this->number_data = new largeinteger_t[(size / 8 + 1) * 8];
    memset(this->number_data, 0, sizeof(uint64_t) * (size / 8 + 1) * 8);
    this->size = size;
    this->sign = sign;
    int i = size - 1;
    while(data[i--] == 0 && i >= 0)
        this->size--;
    for(int i = 0; i < size; ++i )
        this->number_data[i] = data[i];
}

BigInteger::~BigInteger(){
    delete [] number_data;
}

/*
    addition means also signs should be same both plus or both minus
*/
BigInteger* BigInteger::add(BigInteger *numb){
    //decide calculation will be addition or substraction and on who
    if(this->getSign() != numb->getSign()){
        numb->setSign(PLUS); // a + (-b) => a - b or -a + b => -a + b
        return this->substract(numb);
    }
    largeinteger_t *other_numb, *res_arr;
    int other_size, calc_size;
    bool *holds;
    bool is_any_overflow = false;

    other_numb = numb->getData(&other_size);

    calc_size = other_size > this->size ? other_size : this->size;
    res_arr = new largeinteger_t[calc_size + 8]; // can overflow
    holds = new bool[calc_size + 8];

    memset(holds, 0, sizeof(bool) * (calc_size + 8));
    memset(res_arr, 0, sizeof(largeinteger_t) * (calc_size + 8));

#ifndef NONPARALLEL
    #pragma omp parallel for
#endif
    for (int i = 0; i < calc_size; i += 8) {
        holds[i + 7] = add_two_512bit_number(&(res_arr[i]), &this->number_data[i], &other_numb[i]);
        is_any_overflow |= holds[i + 7];
    }
    if(is_any_overflow)
        add_holds_to_number(res_arr, calc_size, holds);
    for(int i = calc_size; i < ((calc_size / 8) + 1) * 8; ++i)
        if(res_arr[i])
            calc_size++;

    BigInteger* temp = new BigInteger(res_arr, calc_size, getSign());

    delete [] holds;
    delete [] res_arr;
    return temp;
}

BigInteger* BigInteger::add(largeinteger_t number){
    largeinteger_t *data;
    largeinteger_t *res;
    int size;
    if(number < 0){
        return substract( -1 * number);
    }
    data = getData(&size);
    res = new largeinteger_t[size + 1];
    res[0] = data[0] + number;
    for(int i = 1; i < size; ++i){
        res[i] = data[i];
        if(res[i - 1] > MAX_NUMBER){
            res[i] -= MAX_NUMBER + 1;
            res[i + 1] += 1;
        }
        else
            break;
    }
    if(res[size - 1] > MAX_NUMBER){
        res[size] += 1;
        size++; 
    }

    return new BigInteger(res, size, getSign());
}

bool checkoverflow(largeinteger_t a , largeinteger_t b){
    if((a + b) < a){
        std::cout << a << " + " << b << std::endl;
        return true;
    }
    return  false;
}
BigInteger* BigInteger::multiply(BigInteger *numb){
    largeinteger_t *other_numb;

    other_numb = (largeinteger_t *)calloc((size_t)((this->getSize() + numb->getSize()) / 8 + 1) * 8, sizeof(largeinteger_t));

    if(this->isZero() || numb->isZero()){
        return CREATE_ZERO;
    }

    memset(other_numb, 0, this->getSize() + numb->getSize());
    largeinteger_t carry = 0;

    //Matrix Mult can be paralelizable
    for (int i = 0; i < this->getSize(); i += 4) {
        for (int j = 0; j < numb->getSize(); j += 4) {
            carry = multiply_two_256bit_number(&(other_numb[i+j]), &(this->getData()[i]), &(numb->getData()[j]));
            other_numb[i + j + 8] += carry;
        }
    }

    return new BigInteger(other_numb, getSize() + numb->getSize(), getSign() == numb->getSign() ? PLUS : MINUS);
}

/**
 * def karatsuba(x,y):

      #base case
      if x < 10 and y < 10: # in other words, if x and y are single digits
        return x*y

       n = max(len(str(x)), len(str(y)))
       m = int(ceil(float(n)/2))   #Cast n into a float because n might lie outside the representable range of integers.

       x_H  = int(floor(x / 10**m))
       x_L = int(x % (10**m))

       y_H = int(floor(y / 10**m))
       y_L = int(y % (10**m))

    #recursive steps
       a = karatsuba(x_H,y_H)
       d = karatsuba(x_L,y_L)
       e = karatsuba(x_H + x_L, y_H + y_L) -a -d

       return int(a*(10**(m*2)) + e*(10**m) + d)
 */
BigInteger * BigInteger::karatsuba_mult(BigInteger *x){
    if(getSize() <= 1 && x->getSize() <= 1){
        uint128_t res;
        uint64_t  temp;
        res = safe_karatsuba_mult(x->getData()[0], this->getData()[0]);
        temp = res.number[0];
        res.number[0] = res.number[1];
        res.number[1] = temp;
        return new BigInteger(res.number, 2 , (getSign() == x->getSign() ? PLUS : MINUS));
    }
    if(getSize() <= 1){
        return x->multiply(getData()[0]);
    }
    if(x->getSize() <= 1){
        return this->multiply(x->getData()[0]);
    }

    int base = 10;
    int digit = (MAX_ORDER) / 2;
    BigInteger lowa(&(this->getData()[this->getSize() / 2]), this->getSize() / 2, this->getSign());
    BigInteger lowb(&(x->getData()[x->getSize() / 2]), x->getSize() / 2, x->getSign());
    BigInteger higha(this->getData(), this->getSize() / 2, this->getSign());
    BigInteger highb(x->getData(), x->getSize() / 2, x->getSign());

//    std::cout << this->toString() << std::endl;
//    std::cout << " Lowa " << lowa.toString() << " Higha " << higha.toString() << std::endl;

   // std::cout << x->toString() << std::endl;
  //  std::cout << " Lowb " << lowb.toString() << " Highb " << highb.toString() << std::endl;

    BigInteger *z2 = higha.karatsuba_mult(&highb);
  //  std::cout << "Z2 : " << std::endl << z2->toString() << std::endl;
    BigInteger *z0 = lowa.karatsuba_mult(&lowb);
  //  std::cout << "Z0 : " << std::endl << z0->toString() << std::endl;
    BigInteger *z1 = higha.add(&lowa)->karatsuba_mult(highb.add(&lowb))->substract(z2)->substract(z0);
   // std::cout << "Z1 : " << std::endl << z1->toString() << std::endl;

    //BigInteger *z1 = (higha + lowa) * (highb + lowb) - z2 - z0;

    //largeinteger_t result = (z2 * pow(bpowem,2)) + (z1 * bpowem) + z0;
    return (z2->multiply(static_cast<largeinteger_t>(pow(10, 16)))->add(z1->multiply(static_cast<largeinteger_t>(pow(10, 8)))))->add(z0);
}


BigInteger* BigInteger::multiply(largeinteger_t numb){
    largeinteger_t *data;
    largeinteger_t carry = 0;
    int size;
    largeinteger_t *res;

    data = getData(&size);
    res = new largeinteger_t[size + 1];
    for (int i = 0; i < size; ++i) {
        res[i] = numb * data[i] + carry;
        if(res[i] > MAX_NUMBER + 1){
            carry = res[i] / (MAX_NUMBER + 1);
            res[i] = res[i] % (MAX_NUMBER + 1);
        }
        else{
            carry = 0;
        }
    }
    if(carry) {
        res[size] = carry;
        size++;
    }
    return new BigInteger(res, size, getSign());
}

compare_result BigInteger::compare(BigInteger *numb){
    int size_other;
    largeinteger_t *data = numb->getData(&size_other);
    if(getSign() == MINUS && numb->getSign() == PLUS)
        return LESS;
    if(getSign() == PLUS && numb->getSign() == MINUS)
        return GREATER;
    if(getSize() > size_other){
        return GREATER;
    }
    if(getSize() < size_other){
        return LESS;
    }

    for(int i = size_other - 1; i >= 0; --i){
        if(data[i] > this->number_data[i])
            return LESS;
        else if(data[i] < this->number_data[i])
            return GREATER;
    }
    return EQUAL;
}

void BigInteger::printDatas(){
    for(int i = 0; i < this->size; ++i)
        printf("%lu  ", this->number_data[i] );
    printf("\n");
}

BigInteger *BigInteger::substract(BigInteger *numb) {
    largeinteger_t *from;
    largeinteger_t *to;
    int from_size, to_size;
    compare_result compare_res;
    SIGN sign_will_be_negative = MINUS;
    int out_size;
    largeinteger_t* out_data;
    int rest_index;
    BigInteger *temp;

    compare_res =  compare(numb);
    if(compare_res == EQUAL)
        return CREATE_ZERO;

    if (this->getSign() == PLUS && numb->getSign() == MINUS) { // a - (-b) => a + b
        numb->setSign(PLUS);
        return this->add(numb);
    } else if (this->getSign() == PLUS && numb->getSign() == MINUS) { // a - b
        from = this->getData(&from_size);
        to = numb->getData(&to_size);
        if(compare_res == LESS){
            largeinteger_t *temp;
            temp = from;
            from = to;
            to = temp;
            from_size = from_size + to_size;
            to_size = from_size - to_size;
            from_size = from_size - to_size;
            sign_will_be_negative = PLUS;
        }
    } else if (this->getSign() == MINUS && numb->getSign() == PLUS) { // -a - b => -(a + b)
        this->setSign(PLUS);
        numb->setSign(PLUS);
        temp= (BigInteger *) this->add(numb);
        temp->setSign(MINUS);
        return temp;
    }else{ // -a - -b => b - a
        from = numb->getData(&from_size);
        to = this->getData(&to_size);
        if(compare_res == GREATER){
            largeinteger_t *temp;
            temp = from;
            from = to;
            to = temp;
            from_size = from_size + to_size;
            to_size = from_size - to_size;
            from_size = from_size - to_size;
            sign_will_be_negative = PLUS;
        }
    }
    /*
     * After theese step we are curtain that bigger number in stored in from pointer
     * and also we know theire sizes. it doesn't matter where they come from, we only 
     * care results. 
     * 
     * from now on we know that informations, 
     *      - from pointer has bigger number
     *      - from size is always big or equal with to_size so we can use it for iterations
     *      - what sign will be
     */
    out_size = from_size;
    out_data =  new largeinteger_t[((out_size / 8) + 1) * 8 ]; // for laziness
    std::vector<int> barrowpos;
    memset(out_data, 0, sizeof(largeinteger_t) * ((out_size / 8) + 1) * 8 );

#ifndef NONPARALLEL
    #pragma omp parallel for
#endif
    for(int i = 0; i < to_size; i += 8 ){
        if(substract_two_512bit_number(&(out_data[i]), &(from[i]), &(to[i])))
            barrowpos.push_back(i+8);

    }

    if(from_size / 8 != to_size / 8){
        for(rest_index = from_size - to_size; rest_index > 0; --rest_index)
            out_data[rest_index] = from[rest_index];
    }

    while(! barrowpos.empty()){
        int pos = barrowpos.back();
        out_data[pos] -= 1;
        barrowpos.pop_back();
    }
    temp = new BigInteger(out_data, out_size, sign_will_be_negative);
    delete out_data;
    return temp;
}

BigInteger *BigInteger::divide(BigInteger *numb) {

    if(numb->isZero()){
        throw std::overflow_error("divide by zero");
    }
    if(this->isZero()){
        return CREATE_ZERO;
    }

    BigInteger counter("0");

    BigInteger *temp = new BigInteger(this->number_data, this->getSize(), PLUS);
    BigInteger *temp2;
    BigInteger *zero = CREATE_ZERO;
    numb->setSign(PLUS);
    compare_result compareResult;
    do{
        temp2 = temp;
        temp = temp->substract(numb);
        delete temp2;
        ++(counter);
        compareResult = temp->compare(zero);
    }while(compareResult == GREATER);
    if(compareResult == LESS)
        --counter;

    delete zero;

    return new BigInteger(counter.number_data, counter.getSize(), getSign());
}

std::string BigInteger::toString() {
    std::string str, number;

    for (int i = 0; i < this->size - 1 ; ++i) {
        number = std::to_string(this->number_data[i]);
        for (int j = MAX_ORDER - number.size() ; j > 0; --j) {
            number.insert(0,"0");
        }
        str.insert(0,number);
    }
    str.insert(0,std::to_string(this->number_data[this->size - 1]));
    if(getSign() == MINUS){
        str.insert(0,"-");
    }
    return str;
}

SIGN BigInteger::getSign() {
    return sign;
}

void BigInteger::setSign(SIGN sign) {
    this->sign = sign;
}

largeinteger_t * BigInteger::getData(int *size) {
    *size = this->size;
    return this->number_data;
}

BigInteger *BigInteger::substract(largeinteger_t number) {
    if (number < 0){
        return add( (-1) *number);
    }
    return substract(new BigInteger(std::to_string(number)));
}

BigInteger *BigInteger::divide(largeinteger_t number) {
    BigInteger *temp = new BigInteger(std::to_string(number));
    BigInteger *res = divide(temp);
    delete temp;
    return res;
}

BigInteger *BigInteger::modulo(BigInteger *numb) {
    if(this->getSign() == MINUS || numb->getSign() == MINUS)
        throw std::runtime_error{"one of them is minus"};
    switch (compare(numb)){
        case LESS:
            return this;
        case EQUAL:
            return CREATE_ZERO;
        case GREATER:break;
    }
    BigInteger* divisionRes = this->divide(numb);
    return this->substract(divisionRes->multiply(numb));
}

largeinteger_t BigInteger::modulo(largeinteger_t numb) {
    if(this->getSign() == false){
        throw std::runtime_error{"Number is minus"};
    }
    largeinteger_t res = this->getData()[0] % numb;
    return res;
}

void BigInteger::pruning() {
    for (int i = 0; i < size; ++i) {
        if(number_data[i] > MAX_NUMBER){
            if(size % 8){
                number_data = (largeinteger_t *) realloc(number_data, sizeof(largeinteger_t) * (size + 8));
            }
            number_data[i] = 0;
            number_data[i+1] += 1;
            size++;
        }
        else
            break;
    }
}
