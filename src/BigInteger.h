#ifndef __BIG_INTEGER_H
#define __BIG_INTEGER_H

#include <immintrin.h>
#include "Utils.h"
#include <string>

class BigInteger {
    public:
        BigInteger(std::string inp);

        BigInteger* add(BigInteger *numb) ;
        BigInteger* add(largeinteger_t number);

        BigInteger* multiply(BigInteger *numb) ;
        BigInteger* multiply(largeinteger_t number);
        BigInteger * karatsuba_mult(BigInteger *x);
        BigInteger* substract(BigInteger *numb) ;
        BigInteger* substract(largeinteger_t number) ;

        BigInteger* divide(BigInteger *numb) ;
        BigInteger* divide(largeinteger_t number);

        BigInteger* modulo(BigInteger *numb);
        largeinteger_t modulo(largeinteger_t numb);

        compare_result compare(BigInteger *numb) ;

        inline BigInteger operator+(BigInteger *numb){return *(this->add(numb));}
        inline BigInteger operator+(largeinteger_t number){return *(this->add(number));}

        inline BigInteger operator-(BigInteger *numb){return *(this->substract(numb));}
        inline BigInteger operator-(largeinteger_t number){return *(this->substract(number));}

        inline BigInteger operator*(BigInteger *numb){return *(this->multiply(numb));}
        inline BigInteger operator*(largeinteger_t number){return *(this->multiply(number));}

        inline BigInteger operator/(BigInteger *numb){return *(this->divide(numb));}
        inline BigInteger operator/(largeinteger_t number){return *(this->divide(number));}

        inline BigInteger operator%(BigInteger *numb){ return *this->modulo(numb);}
        inline largeinteger_t operator%(largeinteger_t number){return (this->modulo(number));}

        inline void operator++(){
            this->number_data[0]++;
            pruning();
        }
        inline void operator--(){
            this->number_data[0]--;
            pruning();
        }

        std::string toString() ;

        SIGN getSign() ;

        void setSign(SIGN sign) ;

        inline bool isZero()  {
            for(int i = 0; i < size; ++i)
                if(number_data[i] != 0)
                    return false;

            return true;
        }
        inline int getSize(){
            return size;
        }

        ~BigInteger();

    private:
        BigInteger(largeinteger_t *data, int size, SIGN sign);
        void printDatas();
        largeinteger_t *getData(int *size);
        largeinteger_t *getData(){
            return number_data;
        }

        void pruning();

    private:
        int base = 10;
        SIGN sign;
        largeinteger_t *number_data;
        int size;
};

#endif