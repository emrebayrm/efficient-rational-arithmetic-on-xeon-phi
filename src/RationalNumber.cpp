//
// Created by patates on 25.11.2018.
//

#include <cmath>
#include "RationalNumber.h"

using namespace std;

RationalNumber::RationalNumber(std::string inp) {
    unsigned long frac_pos = 0;
    string number;

    frac_pos = inp.find('.');
    if(frac_pos != std::string::npos) {
        number = inp.replace(frac_pos,1,""); // one point left
        this->exponential = inp.length() - frac_pos;
    } else{
        number = inp;
        this->exponential = 0;
    }

    this->number = new BigInteger(number);
}

RationalNumber::RationalNumber(BigInteger *integerpart, largeinteger_t expo) {
    this->number = integerpart;
    this->exponential = expo;
}

RationalNumber *RationalNumber::add(RationalNumber *numb) {
    BigInteger *temp, *res;
    if(numb->exponential == this->exponential){
        return new RationalNumber(this->number->add(numb->number), this->exponential);
    }else if (numb->exponential > this->exponential){
        temp = this->number->multiply(static_cast<largeinteger_t>(pow(10, numb->exponential - this->exponential)));
        res = temp->add(numb->number);
        delete temp;
        return new RationalNumber(res, numb->exponential);
    }else{
        temp = numb->number->multiply(static_cast<largeinteger_t>(pow(10, this->exponential - numb->exponential)));
        res = temp->add(this->number);
        delete temp;
        return new RationalNumber(res, this->exponential);
    }
}

RationalNumber *RationalNumber::substract(RationalNumber *numb) {
    BigInteger *temp, *res;
    if(numb->exponential == this->exponential){
        return new RationalNumber(this->number->substract(numb->number), this->exponential);
    }else if (numb->exponential > this->exponential){
        temp = this->number->multiply(static_cast<largeinteger_t>(pow(10, numb->exponential - this->exponential)));
        res = temp->substract(numb->number);
        delete temp;
        return new RationalNumber(res, numb->exponential);
    }else{
        temp = numb->number->multiply(static_cast<largeinteger_t >(pow(10 , this->exponential - numb->exponential)));
        res = temp->substract(numb->number);
        delete temp;
        return new RationalNumber(res, this->exponential);
    }
}

RationalNumber *RationalNumber::multiply(RationalNumber *numb) {
    return new RationalNumber(this->number->multiply(numb->number), this->exponential + numb->exponential);
}

RationalNumber *RationalNumber::divide(RationalNumber *numb) {
    return new RationalNumber(this->number->divide(numb->number), this->exponential - numb->exponential);
}

std::string RationalNumber::toString() {
    string res;
    res = number->toString();
    res.insert(res.length() - exponential,".");
    return res;
}

compare_result RationalNumber::compare(RationalNumber *numb) {
    BigInteger *temp;
    compare_result res;
    if(this->exponential == numb->exponential){
        return this->number->compare(numb->number);
    }
    else if (numb->exponential > this->exponential){
        temp = this->number->multiply(static_cast<largeinteger_t>(pow(10, numb->exponential - this->exponential)));
        res = temp->compare(numb->number);
        delete temp;
        return res;
    }else{
        temp = numb->number->multiply(static_cast<largeinteger_t>(pow(10, this->exponential - numb->exponential)));
        res = temp->compare(this->number);
        delete temp;
        return res;
    }
}

SIGN RationalNumber::getSign() {
    return number->getSign();
}

void RationalNumber::setSign(SIGN sign) {
    number->setSign(sign);
}

bool RationalNumber::isZero() {
    return number->isZero();
}

RationalNumber *RationalNumber::add(BigInteger *numb) {
    RationalNumber *temp;
    temp = new RationalNumber(numb, 0);
    return add(temp);
}

RationalNumber *RationalNumber::substract(BigInteger *numb) {
    RationalNumber *temp;
    temp = new RationalNumber(numb, 0);
    return substract(temp);
}

RationalNumber *RationalNumber::multiply(BigInteger *numb) {
    RationalNumber *temp;
    temp = new RationalNumber(numb, 0);
    return multiply(temp);
}

RationalNumber *RationalNumber::divide(BigInteger *numb) {
    RationalNumber *temp;
    temp = new RationalNumber(numb, 0);
    return divide(temp);
}
