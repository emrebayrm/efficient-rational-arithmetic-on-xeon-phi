//
// Created by patates on 25.11.2018.
//

#ifndef EFFICIENT_RATIONAL_ARITHMETIC_ON_XEON_PHI_RationalNumber_H
#define EFFICIENT_RATIONAL_ARITHMETIC_ON_XEON_PHI_RationalNumber_H

#define LIMITSOFFRACTION 4000

#include "BigInteger.h"
#include <typeinfo>

class RationalNumber {
    public:

        RationalNumber(std::string inp);

        RationalNumber* add(RationalNumber *numb);
        RationalNumber* add(BigInteger *numb);

        RationalNumber* substract(RationalNumber *numb);
        RationalNumber* substract(BigInteger *numb);

        RationalNumber* multiply(RationalNumber *numb);
        RationalNumber* multiply(BigInteger *numb);

        RationalNumber* divide(RationalNumber *numb);
        RationalNumber* divide(BigInteger *numb);

        std::string toString();

        compare_result compare(RationalNumber *numb);

        SIGN getSign();

        void setSign(SIGN sign);

        bool isZero();

    private:
        RationalNumber(BigInteger *integerpart, largeinteger_t expo);

    private:
        BigInteger *number;
        largeinteger_t exponential = 0;
};


#endif //EFFICIENT_RATIONAL_ARITHMETIC_ON_XEON_PHI_RationalNumber_H
