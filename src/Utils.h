#ifndef __UTILS_H
#define __UTILS_H
#include <climits>
#include <cstdint>
#include <immintrin.h>

#define NONAVX
//#define NONPARALLEL
#define MAX_ORDER (8)  // order number

typedef enum { LESS, EQUAL, GREATER} compare_result;

typedef enum { MINUS, PLUS} SIGN;

typedef uint64_t largeinteger_t;

const largeinteger_t MAX_NUMBER = 99999999;

typedef struct{
    largeinteger_t number[2];
}uint128_t;

largeinteger_t hi(largeinteger_t x);

largeinteger_t lo(largeinteger_t x);

uint128_t safe_multiply(largeinteger_t a, largeinteger_t b);

uint128_t safe_karatsuba_mult(largeinteger_t num1 , largeinteger_t num2);

bool substract_two_512bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b);

bool add_two_512bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b);

bool barrow_from_number(largeinteger_t *data, int size, const bool *borrows);

bool add_holds_to_number(largeinteger_t *data, int size, const bool *holds);

bool check_overflow(largeinteger_t number);

bool check_underflow(largeinteger_t number);

largeinteger_t multiply_two_256bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b);

void show_configuration();

#endif