//
// Created by patates on 28.11.2018.
//

#include <cstdio>
#include <cmath>
#include "Utils.h"


bool check_overflow(largeinteger_t number){
    return number > MAX_NUMBER;
}

bool check_underflow(largeinteger_t number){
    return number < 0 | check_overflow(number);
}

void show_configuration(){
    bool nonavx = false, nonparallel = false;
    #ifdef NONAVX
        nonavx = true;
    #endif
    #ifdef NONPARALLEL
        nonparallel = true;
    #endif
    if(nonavx)
        printf("--NONAVX-- ");
    else
        printf("--AVX-- ");
    if(nonparallel)
        printf("--NONPARALLEL--");
    else
        printf("--PARALLEL--");
    printf("\n");
}

bool add_holds_to_number(largeinteger_t *data, int size, const bool *holds){
    for (int i = 0; i < size - 1; ++i) {
        if(holds[i] || check_overflow((data)[i])){
            (data)[i] -= MAX_NUMBER + 1;
            (data)[i + 1] += 1;
        }
    }

    return check_overflow((data)[7]);
}

bool barrow_from_number(largeinteger_t *data, int size, const bool *borrows){
    for (int i = 0; i < size; ++i) {
        if(borrows[i] || check_underflow(data[i])){
            data[i] += MAX_NUMBER + 1;
            data[i + 1] -= 1;
        }
    }
    return check_underflow(data[0]);
}

bool add_two_512bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b){
#ifndef NONAVX
    __m512i simd1, simd2, res;

    simd1 = _mm512_load_epi64(a);
    simd2 = _mm512_load_epi64(b);
    res = _mm512_add_epi64(simd1, simd2);
    _mm512_store_epi64(res_arr, res);

    for(int i = 7; i >= 0; --i ){
        if(check_overflow(res_arr[i])){
            res_arr[i] -= MAX_NUMBER + 1 ;
            res_arr[i - 1] += 1;
        }
    }
#else
    for(int i = 0; i < 7; ++i){
        (res_arr)[i] += a[i] + b[i];
        if(check_overflow((res_arr)[i]) ){
            (res_arr)[i] -= MAX_NUMBER + 1 ;
            res_arr[i + 1] += 1;
        }
    }
    (res_arr)[7] += a[7] + b[7];
#endif
    return check_overflow(res_arr[7]);
}

bool substract_two_512bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b){

#ifndef NONAVX
    __m512i simd1, simd2, res;

    simd1 = _mm512_load_epi64(a);
    simd2 = _mm512_load_epi64(b);
    res = _mm512_sub_epi64(simd1, simd2);
    _mm512_store_epi64(res_arr, res);


    for(int i = 7; i >= 0; --i ){
        if(check_underflow(res_arr[i])){
            res_arr[i] -= MAX_NUMBER ;
            res_arr[i - 1] += 1;
        }
    }
#else
    long long int temp;
    int barrow = 0;
    for(int i = 0; i < 7; ++i){
        if (a[i] - barrow < b[i]){
            res_arr[i] = MAX_NUMBER + 1 + a[i] - b[i] - barrow;
            barrow = 1;
        }
        else {
            res_arr[i] = a[i] - b[i] - barrow;
            barrow = 0;
        }
    }
    temp = static_cast<long long int>(a[7] - b[7] - barrow);

    if ( temp < 0){
        res_arr[7] = (largeinteger_t)( temp + MAX_NUMBER + 1);
        return true;
    }
    else {
        (res_arr)[7] = static_cast<largeinteger_t>(temp);
        return false;
    }
#endif
}
largeinteger_t multiply_two_256bit_number(largeinteger_t *res_arr, const largeinteger_t *a, const largeinteger_t *b){

    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            res_arr[i+j] += (largeinteger_t)a[i] * (largeinteger_t)b[j];
        }
    }

    for (int k = 0; k < 7; ++k) {
        if(res_arr[k] > MAX_NUMBER + 1){
            res_arr[k + 1] += res_arr[k] / (MAX_NUMBER + 1);
            res_arr[k] = res_arr[k] % (MAX_NUMBER + 1);
        }
    }
    largeinteger_t carry = 0;
    if(res_arr[7] > MAX_NUMBER + 1){
        carry = res_arr[7] / (MAX_NUMBER + 1);
        res_arr[7] = res_arr[7] % (MAX_NUMBER + 1);
    }
    return carry;
}
largeinteger_t hi(largeinteger_t x) {
    return x >> 32;
}

largeinteger_t lo(largeinteger_t x) {
    return ((1L << 32) - 1) & x;
}

uint128_t safe_multiply(largeinteger_t a, largeinteger_t b) {
    // actually uint32_t would do, but the casting is annoying
    largeinteger_t s0, s1, s2, s3;
    uint128_t res;
    largeinteger_t x = lo(a) * lo(b); //  x => z0
    s0 = lo(x);

    x = hi(a) * lo(b) + hi(x);
    s1 = lo(x);
    s2 = hi(x);

    x = s1 + lo(a) * hi(b);
    s1 = lo(x);

    x = s2 + hi(a) * hi(b) + hi(x);
    s2 = lo(x);
    s3 = hi(x);

    res.number[0] = s1 << 32 | s0;
    res.number[1] = s3 << 32 | s2;

    return res;
}

uint128_t safe_karatsuba_mult(largeinteger_t  num1 , largeinteger_t num2){
    int base = 10;
    int digit = (MAX_ORDER) / 2;
    uint128_t res;
    largeinteger_t bpowem = (largeinteger_t) pow(base, digit);
    largeinteger_t lowa = num1 % bpowem ;
    largeinteger_t lowb = num2 % bpowem;
    largeinteger_t higha = num1 / bpowem;
    largeinteger_t highb = num2 / bpowem;

    largeinteger_t z2 = higha * highb;
    largeinteger_t z0 = lowa * lowb;
    largeinteger_t z1 = (higha + lowa) * (highb + lowb) - z2 - z0;

    //largeinteger_t result = (z2 * pow(bpowem,2)) + (z1 * bpowem) + z0;
    largeinteger_t carry;
    carry = z2 + (z1 / bpowem);
    largeinteger_t result;
    result = (z1 % bpowem) * bpowem + z0;
    
    if(result > MAX_NUMBER){
        carry = carry + (result / bpowem);
        result = result % (bpowem) ;
    }
    res.number[0] = carry;
    res.number[1] = result;

    return res;
}