#include <iostream>
#include <chrono>
#include "BigInteger.h"
#include "RationalNumber.h"

using HRC = std::chrono::high_resolution_clock;
using namespace std;

void show_two_diff_string(string a, string b){
    int end = (int) min(a.size(), b.size());
    string line1,line2;
    for (int i = 0; i < end; ++i) {
        if(a[i] != b[i]){
            line1.push_back('-');
            line1.push_back(a[i]);
            line1.push_back('-');
            line2.push_back('-');
            line2.push_back(b[i]);
            line2.push_back('-');
        }
        else{
            line1.push_back(a[i]);
            line2.push_back(b[i]);
        }
    }
    cout << line1 << endl;
    cout << line2 << endl;
}

uint64_t test_init(){
    HRC::time_point t1;
    uint64_t duration;
    string test_str = "-1234567878971237763762376321762166124570439852398120463427561739812649716239127361293638475619283719274978979878964513213216158879789";
    string getstr;
    BigInteger *b1;

    cout << "init test " << endl;

    t1 = HRC::now();
    b1 = new BigInteger(test_str);
    getstr = b1->toString();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(HRC::now() - t1).count();

    if(getstr == test_str){
        cout << "OK";
    } else{
        cout << "FAIL"<< endl
             << "Expected : " << test_str << " but get : " << getstr;
    }
    return duration;
}

bool test_add(string header, BigInteger *param1, BigInteger *param2, BigInteger *expected){
    BigInteger *temp;
    cout << "Testing : " << header << endl << endl;
    temp = (param1->add(param2));
    if(temp->compare(expected) == EQUAL){
        cout << "OK" << endl << endl;
        delete temp;
        return true;
    }
    else{

        cout << "FAIL" << endl;
        cout << "Expected : " << expected->toString() << endl
             << "Get      : " << temp->toString() << endl << endl;
        show_two_diff_string(temp->toString(), expected->toString());
        delete temp;
        return false;
    }

}

uint64_t test_addition(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();
    test_add("Add with zero",
            new BigInteger("1231243435653524565432145678765423456789765432"),
            new BigInteger("0"),
            new BigInteger("1231243435653524565432145678765423456789765432"));
    test_add("Add with normal",
             new BigInteger("1231243435653524565432145678765423456789765432"),
             new BigInteger("1231243435653524565432145678765423456789765432"),
             new BigInteger("2462486871307049130864291357530846913579530864"));
    test_add("Add with negative",
             new BigInteger("1231243435653524565432145678765423456789765432"),
             new BigInteger("-1231243435653524565432145678765423456789765432"),
             new BigInteger("0"));
    test_add("Add with both negative",
             new BigInteger("-1231243435653524565432145678765423456789765432"),
             new BigInteger("-1231243435653524565432145678765423456789765432"),
             new BigInteger("-2462486871307049130864291357530846913579530864"));
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;
}

template <class T>
bool test_sub(string header, T *param1, T *param2, T *expected){
    T *temp;
    cout << "Testing : " << header << endl << endl;
    temp = (param1->substract(param2));
    if(temp->toString() == expected->toString()){
        cout << "OK" << endl << endl;
        delete temp;
        return true;
    }
    else{

        cout << "FAIL" << endl;
        cout << "Expected : " << expected->toString() << endl
             << "Get      : " << temp->toString() << endl << endl;
        show_two_diff_string(temp->toString(), expected->toString());
        delete temp;
        return false;
    }

}

uint64_t test_substraction(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();

    test_sub("LESS ", new BigInteger("22222222"), new BigInteger("11111111"), new BigInteger("11111111"));
    test_sub("LESS ", new BigInteger("13265"), new BigInteger("8798798798798"), new BigInteger("-8798798785533"));
    test_sub("GREAT NORMAL ", new BigInteger("712356457981324987461531268"), new BigInteger("8798798798798"), new BigInteger("712356457981316188662732470"));
    test_sub("EQUAL ", new BigInteger("879778946561332215354667898465123549874613246879465123"), new BigInteger("879778946561332215354667898465123549874613246879465123"), new BigInteger("0"));
    test_sub("MINUS ", new BigInteger("13265"), new BigInteger("-8798798798798"), new BigInteger("8798798812063"));
    test_sub("REAL BIGGG ",
             new BigInteger("12389164782146192837145261982364972156837618026419283614021238126498124710247128047124185123709142561023974832642934871209376412975683475610239127436932586471029374"),
              new BigInteger("2873452837496128362734283745162983724823591283078428734612839612745211038271647536879806238064237462834574527346293481029389765875128362834629384719264192846012846"),
             new BigInteger("9515711944650064474410978237201988432014026743340854879408398513752913671975480510244378885644905098189400305296641390179986647100555112775609742717668393625016528"));

    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;
}

bool test_karatsuba(string header, BigInteger *param1, BigInteger *param2, BigInteger *expected){
    BigInteger *temp;
    cout << "Testing : " << header << endl << endl;
    temp = (param1->multiply(param2));
    if(temp->compare(expected) == EQUAL){
        cout << "OK" << endl << endl;
        delete temp;
        return true;
    }
    else{

        cout << "FAIL" << endl;
        cout << "Expected : " << expected->toString() << endl
             << "Get      : " << temp->toString() << endl << endl;
        show_two_diff_string(temp->toString(), expected->toString());
        delete temp;
        return false;
    }

}

uint64_t test_multiplication(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();
    test_karatsuba("With 0 multiplication",
                   new BigInteger("1241241212312512512381728301283921074124512"),
                   new BigInteger("0"),
                   new BigInteger("0"));
    test_karatsuba("Little numbers",
                   new BigInteger("56"),
                   new BigInteger("100000000"),
                   new BigInteger("5600000000"));
    test_karatsuba("Normal Big Number",
                   new BigInteger("2947352985712497502937428356378456192346258736412893782963408539465710294695372640137403857693476501238412341256341624712948712984725346553453345"),
                   new BigInteger("316253416523491827312421323514532745668763894612312333254386674565233529834729380497135730485729347349856329847293561982346623853265"),
                   new BigInteger("932110451432291729420787887166412588293610651613057773741057062493932656699851446410583364303393930932005792821273930098447467355953172051036931478302086537749363634880580891519003197841602960504222324758383276570651499325908912829683248645431026140444886612800275736303421425"));
    test_karatsuba("Negative",
                   new BigInteger("219387419725612804792735628374619246912864917356102846189204712056812975236"),
                   new BigInteger("-123871982456128374021561029564298347603816274528348710258239784710248974531256874632156879452130000"),
                   new BigInteger("-27175954607346381334484774935208501955963004211035721350581492440797060526287681932430593129722967325821208680952131936553794727779064202828453603970459838140898937452680000"));
    test_karatsuba("both negative",
                   new BigInteger("-248972398265304812974012"),
                   new BigInteger("-981237192865873246238984712849236592837401284"),
                   new BigInteger("244300977174931904416589548550354325617144098503716958482921507431408"));
    test_karatsuba("edge case",
                   new BigInteger("999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999"),
                   new BigInteger("999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999"),
                   new BigInteger("999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999998000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001"));
    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;
}

bool test_div(string header, BigInteger *param1, BigInteger *param2, BigInteger *expected){
    BigInteger *temp;
    cout << "Testing : " << header << endl << endl;
    temp = (param1->divide(param2));
    if(temp->compare(expected) == EQUAL){
        cout << "OK" << endl << endl;
        delete temp;
        return true;
    }
    else{

        cout << "FAIL" << endl;
        cout << "Expected : " << expected->toString() << endl
             << "Get      : " << temp->toString() << endl << endl;
        show_two_diff_string(temp->toString(), expected->toString());
        delete temp;
        return false;
    }

}

uint64_t test_division(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();

    test_div("1 / 1 = 1 simple div",
            new BigInteger("1"),
            new BigInteger("1"),
            new BigInteger("1"));
    test_div("0 / 1 = 0 simple div denum",
            new BigInteger("0"),
            new BigInteger("1"),
            new BigInteger("0"));
    try {

        test_div("a number / 0 exception",
            new BigInteger("231"),
            new BigInteger("0"),
            new BigInteger("-0"));
    }
    catch(std::overflow_error &e){
        std::cout << e.what() << endl;
    }
    test_div("normal division little numbers",
            new BigInteger("22222222"),
            new BigInteger("11111111"),
            new BigInteger("2"));
    test_div("big division 1",
            new BigInteger("9789946513264897465123647984613235749845612349874631329789641325649874654654654612332465798654654613213216549978979846513213245678946513216546798463213216879846"),
            new BigInteger("23126456786453123548794651323216579846123654986513"),
            new BigInteger("423322370723023771761962899124122324435954147000089063849370394248485256383986545120522857822203248576800227328"));
    test_div("big division 2",
             new BigInteger("23126456786453123548794651323216579846123654986513"),
             new BigInteger("9789946513264897465123647984613235749845612349874631329789641325649874654654654612332465798654654613213216549978979846513213245678946513216546798463213216879846"),
             new BigInteger("0"));

    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;

}

uint64_t test_init_Rational(){
    HRC::time_point t1;
    uint64_t duration;
    string test_str = "-12345678789712377637623763217621661245704398523981204634275617398.0000012649716239127361293638475619283719274978979878964513213216158879789";
    string getstr;
    RationalNumber *b1;

    cout << "init test " << endl;

    t1 = HRC::now();
    b1 = new RationalNumber(test_str);
    getstr = b1->toString();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(HRC::now() - t1).count();

    if(getstr == test_str){
        cout << "OK";
    } else{
        cout << "FAIL"<< endl
             << "Expected : " << test_str << " but get : " << getstr;
    }
    return duration;
}

bool test_add_Rational(string header, RationalNumber *param1, RationalNumber *param2, RationalNumber *expected){
    RationalNumber *temp;
    cout << "Testing : " << header << endl << endl;
    temp = (param1->add(param2));
    if(temp->toString() == expected->toString()){
        cout << "OK" << endl << endl;
        delete temp;
        return true;
    }
    else{

        cout << "FAIL" << endl;
        cout << "Expected : " << expected->toString() << endl
             << "Get      : " << temp->toString() << endl << endl;
        show_two_diff_string(temp->toString(), expected->toString());
        delete temp;
        return false;
    }

}

uint64_t test_addition_Rational(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();
    test_add_Rational("Add with zero",
             new RationalNumber("12312434356535245654.32145678765423456789765432"),
             new RationalNumber("0"),
             new RationalNumber("12312434356535245654.32145678765423456789765432"));
    test_add_Rational("Add with normal",
             new RationalNumber("123124343565352.4565432145678765423456789765432"),
             new RationalNumber("123124343565352456543.2145678765423456789765432"),
             new RationalNumber("123124466689696021895.6711110911102222213222221765432"));
    test_add_Rational("Add with negative",
             new RationalNumber("1231243435653524565432145678765423456789765432"),
             new RationalNumber("-1231243435653524565432145678765423456789765432"),
             new RationalNumber("0"));
    test_add_Rational("Add with both negative",
             new RationalNumber("-1231243435653524565432145678765423456789765432"),
             new RationalNumber("-1231243435653524565432145678765423456789765432"),
             new RationalNumber("-2462486871307049130864291357530846913579530864"));

    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;
}

uint64_t test_substraction_rational(){
    HRC::time_point t1;
    uint64_t duration;
    t1 = HRC::now();

    test_sub("LESS ", new RationalNumber("22222222.0"), new RationalNumber("11111111.0"), new RationalNumber("11111111.0"));
    test_sub("LESS ", new RationalNumber("1326.5"), new RationalNumber("879879879879.8"), new RationalNumber("-879879878553.3"));
    test_sub("GREAT NORMAL ", new RationalNumber("71235645798132498.7461531268"), new RationalNumber("87.98798798798"), new RationalNumber("71235645798132410.75816513882"));
    test_sub("EQUAL ", new RationalNumber("879778946561332215354667898465123549874613246879465123"), new RationalNumber("879778946561332215354667898465123549874613246879465123"), new RationalNumber("0"));
    test_sub("MINUS ", new RationalNumber("13265"), new RationalNumber("-8798798798798"), new RationalNumber("8798798812063"));
    test_sub("REAL BIGGG ",
             new BigInteger("12389164782146192837145261982364972156837618026419283614021238126498124710247128047124185123709142561023974832642934871209376412975683475610239127436932586471029374"),
             new BigInteger("2873452837496128362734283745162983724823591283078428734612839612745211038271647536879806238064237462834574527346293481029389765875128362834629384719264192846012846"),
             new BigInteger("9515711944650064474410978237201988432014026743340854879408398513752913671975480510244378885644905098189400305296641390179986647100555112775609742717668393625016528"));

    duration = std::chrono::duration_cast<std::chrono::nanoseconds>(HRC::now() - t1).count();

    return duration;
}


int main() {
    uint64_t duration;

    duration = test_init();
    cout << "###############     init test " << duration << endl;
    duration = test_addition();
    cout << "###############     addtion test " << duration << endl;
    duration = test_substraction();
    cout << "###############     substraction test " << duration << endl;
    duration = test_multiplication();
    cout << "###############     multiplication test " << duration << endl;
    duration = test_division();
    cout << "###############     multiplication test " << duration << endl;

    duration = test_init_Rational();
    test_addition_Rational();

    test_substraction_rational();
    return 0;
}